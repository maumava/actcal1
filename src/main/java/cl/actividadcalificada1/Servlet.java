package cl.actividadcalificada1;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.System.out;

@WebServlet(name = "Servlet", value = "/resultado")
public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String curso = request.getParameter("curso");

        out.println("<html>");
        out.println("<title>Formulario Alumno</title>");

        out.println("<body>");
        out.println("<p>Nombre: " + nombre + "</p>");
        out.println("<p>Appelido: " + apellido + "</p>");
        out.println("<p>Curso: " + curso + "</p>");
        out.println("</body>");
        out.println("</html>");


    }
}
